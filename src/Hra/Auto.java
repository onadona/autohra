package Hra;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;

/**
 * T��da reprezentuj�c� auto.
 * 
 * @author vita
 */
public class Auto implements KeyListener {
	private HerniPanel panel; // reference na panel
	private Image autoObr;
	private int x; // x-ov� sou�adnice auta
	private int y; // y-ov� sou�adnice auta
	private int dx; // sm�r auta po ose x (+ doprava, - doleva)

	/**
	 * Kontruktor t��dy Auto
	 * 
	 * @param sirkaPanelu - ���ka hern� plochy
	 */
	public Auto(HerniPanel panel) {

		ImageIcon ii = new ImageIcon(this.getClass().getResource("auto.png"));
		autoObr = ii.getImage();

		this.panel = panel;
		this.x = 185; // po��te�n� sou�adnice x
		this.y = 558; // sou�adnice y
		this.dx = 0;
	}

	/**
	 * Vykresl� obr�zek na aktu�ln� sou�adnice
	 * 
	 * @param g grafick� kontext
	 */
	public void vykresliSe(Graphics g) {

		g.drawImage(autoObr, x, y, null);
	}

	/**
	 * Zm�na x-ov� sou�adnice v dan�m sm�ru.
	 */
	public void provedPohyb() {
		x += dx;
		if (x < 0) {
			x = 0;
		} else if (x > (panel.getWidth() - autoObr.getWidth(null) - 1) && (panel.getWidth() > 0)) {
			x = panel.getWidth() - autoObr.getWidth(null) - 1;
		}
	}

	/**
	 * Vrac� obrys obr�zku ve form� obd�ln�ka.
	 * 
	 * @return Rectangle ve velikosti obr�zku
	 */
	public Rectangle getOkraje() {
		Rectangle r = new Rectangle(x, y, autoObr.getWidth(null), autoObr.getHeight(null));
		return r;
	}

	/**
	 * Definuje �innost, kter� se provede po stisku kl�vesy na kl�vesnici.
	 * 
	 * @param e - KeyEvent
	 */
	@Override
	public void keyPressed(KeyEvent ke) {
		int key = ke.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			dx = -2; // sm�r doleva, rychlost 2px za vykreslen�
		}

		if (key == KeyEvent.VK_RIGHT) {
			dx = 2; // sm�r doprava, rychlost 2px za vykreslen�
		}
	}

	/**
	 * Definuje �innost, kter� se provede po pu�t�n� stisknut� kl�vesy.
	 * 
	 * @param e - KeyEvent
	 */
	@Override
	public void keyReleased(KeyEvent ke) {
		dx = 0;
	}

	@Override
	public void keyTyped(KeyEvent ke) {
	}
}