package Hra;

import javax.swing.JFrame;

/**
 * T��da obsahuj�c� hlavn� okno programu a metodu main.
 * @author vita
 */
public class AutoHra extends JFrame {

    /**
     * Konstruktor hlavn�ho okna programu.
     */
    public AutoHra() {
        this.setTitle("AutoHra");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        HerniPanel panel = new HerniPanel();
        this.add(panel);

        this.setResizable(false);
        this.pack();
    }

     /**
      * Vstupn� bod programu.
      * Vytvo�� hlavn� okno s programem.
      * @param args - v tomto programu nepou��v�m
      */
    public static void main(String[] args) {
    	AutoHra hlavniOkno = new AutoHra();
        hlavniOkno.setVisible(true);
        
    }
}