package Hra;

import java.awt.Color;

import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javafx.fxml.*;

import java.io.*;
import java.net.*;
import java.util.*;
import javafx.application.*;
import javafx.beans.binding.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.stage.*;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;

/**
 * T��da p�edstavuj�c� hern� panel. D�d� z JPanelu a implementuje rozhran� pro
 * naslouch�n� ud�lostem ActionEvent
 * 
 * @author vita
 */
public class HerniPanel extends JPanel implements ActionListener {
	private static final JToolBar JToolBar = null;
	private static final Event MouseEvent = null;
	private static final JMenuBar JMenuBar = null;
	private int sirkaPanelu = 400; // preferovan� ���ka panelu
	private int vyskaPanelu = 600; // preferovan� v��ka panelu
	private Timer casovac;
	private int score; // pocita pocet dobrych prekazek
	private Auto auto;
	private boolean hrajeSe; // true kdy� se hraje, false kdy� hra skon�ila
	private int citac; // po��t� body, ��m d�le se hraje, t�m v�ce
	private int prodlevaMeziPrekazkami; // jak� je prodleva, ne� se p�id� dal�� p�ek�ka
	private int prodlevaMeziBodiky;
	private List<Prekazka> prekazky; // seznam p�ek�ek
	private List<Bodiky> bodiky;
	private Object menu;
	private JButton konec;
	private JButton znova;
	private JButton spustit;
	private String jmeno;
	private JButton zapisSkore;

	/**
	 * Konstruktor HernihoPanelu.
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public HerniPanel() {
		hrajeSe = true;
		init();
		start();

		JMenu menu = new JMenu("Menu");
		menu.add("PauzaPauzaPauza").addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (hrajeSe) {
					casovac.stop();
					spustit.setVisible(true);
					zastav();
				}
			}
		});
		menu.add("Konec").addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		menu.add("Sk�re").addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				vypisScore();
			}
		});

		JMenuBar JMenuBar = new javax.swing.JMenuBar();
		JMenuBar.setBounds(0, 0, sirkaPanelu, 20);
		this.add(JMenuBar);
		JMenuBar.add(menu);

		spustit = new JButton("Spustit");

		// spustit.setBounds(150, 300, 100, 60);
		this.add(spustit);
		spustit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spustit.setVisible(false);
				casovac.start();
				spust();

			}

		});
		spustit.setVisible(false);

		konec = new JButton("Konec");
		this.add(konec);
		konec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});
		konec.setVisible(false);

		znova = new JButton("Zkusit znova");
		this.add(znova);
		znova.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				znova.setVisible(false);
				konec.setVisible(false);

				hrajeSe = true;
				citac = 0;
				score = 0;
				start();

			}
		});
		znova.setVisible(false);

		zapisSkore = new JButton("Zapis Skore");
		this.add(zapisSkore);
		zapisSkore.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		zapisSkore.setVisible(false);
	}

	/**
	 * Slou�� k inicializaci HernihoPanelu
	 */
	private void init() {
		this.setPreferredSize(new Dimension(sirkaPanelu, vyskaPanelu));
		this.setBackground(Color.black);
		this.setForeground(Color.white);
		this.setFont(new Font(Font.MONOSPACED, Font.BOLD, 20));
		this.setFocusable(true);

	}

	/**
	 * Provede inicializaci prom�nn�ch nutn�ch ke h�e a spust� timer.
	 */
	private void start() {
		auto = new Auto(this);
		prodlevaMeziPrekazkami = 10;
		prodlevaMeziBodiky = 100;
		prekazky = new ArrayList<Prekazka>();
		bodiky = new ArrayList<Bodiky>();
		this.addKeyListener(auto);

		casovac = new Timer(10, this);
		casovac.start();

	}

	/**
	 * Zde definujeme, co se m� d�t p�i vykreslov�n� HernihoPanelu.
	 * 
	 * @param g grafick� kontext
	 */
	@Override
	public void paintComponent(Graphics g) {

		// pokud se hraje
		if (hrajeSe) {
			super.paintComponent(g);
			vypisCitac(g);
			// vypisMenu(JMenuBar);
			auto.vykresliSe(g);

			for (int i = 0; i < prekazky.size(); i++) {
				Prekazka prek = prekazky.get(i);
				prek.vykresliSe(g);
			}

			for (int i = 0; i < bodiky.size(); i++) {
				Bodiky bod = bodiky.get(i);
				bod.vykresliSe(g);
			}

		} else {
			if (isSrazka()) {
				vypisKonec(g);
				konec.setVisible(true);
				znova.setVisible(true);
				spustit.setVisible(false);
			}
		}

	}

	private void vypisScore() {
		String[][] data = {{"adam","1"},{"kaja","2"},{"alena","3"}};
		String[] columNames = {"name", "skore"};
		JTable skore = new JTable(data, columNames);
		skore.setBounds(30, 40, 200, 300);
	}

	/**
	 * Vyp�e na panel stav ��ta�e
	 * 
	 * @param g grafick� kontext
	 */
	private void vypisCitac(Graphics g) {
		g.drawString(String.valueOf(citac), 10, 40);
	}

	/**
	 * V p��pad� ukon�en� hry vyp�e z�v�re�n� text.
	 * 
	 * @param g grafick� kontext
	 */
	public void vypisKonec(Graphics g) {
		String textKonec = "G A M E   O V E R"; // text, kter� se vyp�e p�i skon�en� hry
		Font pismo = new Font(Font.MONOSPACED, Font.BOLD, 28);
		g.setFont(pismo);
		FontMetrics fm = g.getFontMetrics(pismo);
		int sirkaTextu = fm.stringWidth(textKonec);

		g.setColor(Color.red); // tento text bude �erven�
		g.drawString(textKonec, (this.getWidth() - sirkaTextu) / 2, this.getHeight() / 2);

	}

//	

	/**
	 * Definuje, co se m� st�t p�i vzniku ud�losti ActionEvent, kterou generuje
	 * Timer.
	 * 
	 * @param ae ud�lost ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {

		citac++; // zv��� ��ta� o jedni�ku

		auto.provedPohyb();
		pridejPrekazku();
	//	pridejBodiky();
		pohniPrekazkami();
		pohniBodiky();

		if (isSrazka()) {
			hrajeSe = false;
			casovac.stop();
		}

		if (Pridej()) {

			score++;
		}

		odstranPrekazkyKtereJsouMimo();
		odstranBodikyKtereJsouMimo();

		this.repaint(); // metoda, kter� p�ekresl� panel
	}

	public void ukazSkore() throws IOException {
		/*
		 * score dokument
		 */
		File dir = new File("C:\\Users\\rejlkovachr1\\eclipse-workspace");
		File file = new File(dir, "score.txt");
		FileWriter score = new FileWriter(file);
		score.write(String.format("%20s %20s \r\n", "JM�NO", "SK�RE"));
		score.write(String.format("%20s %20s \r\n", jmeno, score));

		score.flush();

	}

	/**
	 * Odstran� p�ek�ky, kter� ji� nejsou viditeln�.
	 */
	private void odstranPrekazkyKtereJsouMimo() {
		for (int i = 0; i < prekazky.size(); i++) {
			Prekazka prek = prekazky.get(i);
			if (prek.isViditelny() == false) {
				prekazky.remove(prek);
			}
		}

	}

	private void odstranBodikyKtereJsouMimo() {
		for (int i = 0; i < bodiky.size(); i++) {
			Bodiky bod = bodiky.get(i);
			if (bod.isViditelny() == false) {
				bodiky.remove(bod);
			}
		}

	}

	/*
	 * p�id� bod�ky
	 */

	private boolean Pridej() {

		for (int i = 0; i < bodiky.size(); i++) {
			Bodiky bod = bodiky.get(i);
			if (auto.getOkraje().intersects(bod.getOkreje())) {
				bodiky.remove(bod);
				return true;
			}
		}
		return false;
	}

	/**
	 * Zjist�, zda do�lo ke sr�ce.
	 * 
	 * @return true pokud ano, fale pokud ne
	 */
	private boolean isSrazka() {
		for (int i = 0; i < prekazky.size(); i++) {
			Prekazka prek = prekazky.get(i);
			if (auto.getOkraje().intersects(prek.getOkreje())) {
				return true;
			}
		}

		return false;
	}

	/*
	 * zastaveni hry
	 */

	public boolean zastav() {
		for (int i = 0; i < prekazky.size(); i++) {
			Prekazka prek = prekazky.get(i);
			if (auto.getOkraje().intersects(prek.getOkreje())) {
				return true;
			}
		}
		return false;
	}

	/*
	 * spusteni z pauzy
	 */

	private boolean spust() {
		for (int i = 0; i < prekazky.size(); i++) {
			Prekazka prek = prekazky.get(i);
			if (auto.getOkraje().intersects(prek.getOkreje())) {
				return false;
			}
		}
		return true;

	}

	/**
	 * Provede pohyb v�ech p�ek�ek, kter� jsou v seznamu p�ekazky.
	 */
	private void pohniPrekazkami() {
		for (int i = 0; i < prekazky.size(); i++) {
			Prekazka prek = prekazky.get(i);
			prek.provedPohyb();
		}
	}

	private void pohniBodiky() {
		for (int i = 0; i < bodiky.size(); i++) {
			Bodiky bod = bodiky.get(i);
			bod.provedPohyb();
		}
	}

	/**
	 * Zkontroluje prodlevu a v p��pad�, �e je �as, tak p�id� dal�� p�ek�ku. S
	 * pokro�ilost� hry se prodleva zmen�uje a p�ek�ek p�ib�v�.
	 */
	private void pridejPrekazku() {
		if (citac == 1000) {
			prodlevaMeziPrekazkami = 70;
		}
		if (citac == 2000) {
			prodlevaMeziPrekazkami = 50;
		}
		if (citac == 3000) {
			prodlevaMeziPrekazkami = 40;
		}
		if (citac == 5000) {
			prodlevaMeziPrekazkami = 30;
		}
		if (citac == 7500) {
			prodlevaMeziPrekazkami = 20;
		}
		if (citac == 10000) {
			prodlevaMeziPrekazkami = 10;
		}

		// pokud je zbytek po d�len� 0, p�id� dal�� p�ek�ku
		if ((citac % prodlevaMeziPrekazkami) == 0) {
			Prekazka prek = new Prekazka(this);
			prekazky.add(prek);
		}
	}

//	private void pridejBodiky() {
//		if (citac == 1000) {
//			prodlevaMeziBodiky = 500;
//		}
//		if (citac == 2000) {
//			prodlevaMeziBodiky = 400;
//		}
//		if (citac == 3000) {
//			prodlevaMeziBodiky = 400;
//		}
//		if (citac == 5000) {
//			prodlevaMeziBodiky = 400;
//		}
//		if (citac == 7500) {
//			prodlevaMeziBodiky = 400;
//		}
//		if (citac == 10000) {
//			prodlevaMeziBodiky = 1000;
//		}
//
//		// pokud je zbytek po d�len� 0, p�id� dal�� bod�ky
//		if ((citac % prodlevaMeziBodiky) == 0) {
//			Bodiky bod = new Bodiky(this);
//			bodiky.add(bod);
//		}
//	}

}
