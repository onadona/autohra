package Hra;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 * T��da reprezentuj�c� p�ek�ku.
 * 
 * @author vita
 */
public class Prekazka extends Thread {
	private HerniPanel panel; // reference na panel
	private Image prekazkaObr;
	private int x;
	private int y;
	private int dy = 1; // sm�r na ose y (dol�) a rychlost (1px za vykreslen�)
	private boolean viditelny; // p�ek�ka je na panelu true, mimo panel false
	private Random generator; // gener�tor n�hodn�ch ��sel

	/**
	 * Konstruktor.
	 * 
	 * @param x x-ov� sou�adnice
	 */
	public Prekazka(HerniPanel panel) {
		ImageIcon ii = new ImageIcon(this.getClass().getResource("prekazka.png"));
		prekazkaObr = ii.getImage();

		this.generator = new Random(); // vytvo�en� gener�toru n�hodn�ch ��sel
		this.panel = panel; // reference na panel
		this.x = generator.nextInt(panel.getWidth() - prekazkaObr.getWidth(null));
		this.y = -50; // um�st�n� p�ek�ky na ose y p�i vytvo�en�

		viditelny = true;
	}

	/**
	 * Vykresl� obr�zek na aktu�ln� sou�adnice
	 * 
	 * @param g grafick� kontext
	 */
	public void vykresliSe(Graphics g) {

		g.drawImage(prekazkaObr, x, y, null);
	}

	/**
	 * Zm�na y-ov� sou�adnice. Pokud dojede na konec panelu, zm�n� se hodnota
	 * viditelny na false.
	 */
	public void provedPohyb() {
		y += dy;
		if (y > panel.getHeight()) {
			viditelny = false;
		}
	}

	/**
	 * Vrac� obrys obr�zku ve form� obd�ln�ka.
	 * 
	 * @return Rectangle ve velikosti obr�zku
	 */
	public Rectangle getOkreje() {
		Rectangle r = new Rectangle(x, y, prekazkaObr.getWidth(panel), prekazkaObr.getHeight(panel));
		return r;
	}

	/**
	 * Vrac� zda je objekt visible �i nikoli.
	 * 
	 * @return visible
	 */
	public boolean isViditelny() {
		return viditelny;
	}

}