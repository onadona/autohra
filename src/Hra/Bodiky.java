package Hra;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Random;
import javax.swing.ImageIcon;

public class Bodiky extends Prekazka {

	private HerniPanel panel; // reference na panel
	private Image bodikyObr;
	private int x;
	private int y;
	private int dy = 1; // sm�r na ose y (dol�) a rychlost (1px za vykreslen�)
	private boolean viditelny; // p�ek�ka je na panelu true, mimo panel false
	private Random generator; // gener�tor n�hodn�ch ��sel

	public Bodiky(HerniPanel panel) {

		super(panel);
		// TODO Auto-generated constructor stub
		ImageIcon ii = new ImageIcon(this.getClass().getResource("bodiky.png"));
		bodikyObr = ii.getImage();

		this.generator = new Random(); // vytvo�en� gener�toru n�hodn�ch ��sel
		this.panel = panel; // reference na panel
		this.x = generator.nextInt(panel.getWidth() - bodikyObr.getWidth(null));
		this.y = -50; // um�st�n� p�ek�ky na ose y p�i vytvo�en�

		viditelny = true;
	}

	public void provedPohyb() {

		y += dy;
		if (y > panel.getHeight()) {
			viditelny = false;
		}

	}

	public Rectangle getOkreje() {
		Rectangle r = new Rectangle(x, y, bodikyObr.getWidth(panel), bodikyObr.getHeight(panel));
		return r;
	}

	public boolean isViditelny() {
		return viditelny;
	}

	public void vykresliSe(Graphics g) {
		// TODO Auto-generated method stub

		g.drawImage(bodikyObr, x, y, null);
	}

}
